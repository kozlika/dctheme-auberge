// Tâches Gulp

const { src, dest, watch, series, parallel } = require('gulp');

const sass = require('gulp-sass');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const terser = require('gulp-terser');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const replace = require('gulp-replace');


// Chemins
const files = {
    scssPath: 'sources/scss/**/*.scss',
    jsPath: 'sources/scripts/**/*.js'
}

// Tâche sass : compilation, autoprefixer, génération de la sourcemap
function makeCSS(){
    return src(files.scssPath)
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(sourcemaps.write('.'))
        .pipe(dest('.')
    );
}

// Tâche js : concatène les scripts dans un seul fichier
function makeJS(){
    return src([
        files.jsPath
        ])
        .pipe(concat('auberge.js'))
        .pipe(terser())
        .pipe(dest('.')
    );
}

// Surveillance des modifs sur les js ou les scss
function watchMake(){
    watch([files.scssPath, files.jsPath],
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel(makeCSS, makeJS)
        )
    );
}

// Tâche par défaut
exports.default = series(
    parallel(makeCSS, makeJS),
    watchMake
);
