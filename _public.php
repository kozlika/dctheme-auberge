<?php
namespace themes\berlin;

if (!defined('DC_RC_PATH')) {return;}

\l10n::set(dirname(__FILE__) . '/locales/' . $_lang . '/main');
