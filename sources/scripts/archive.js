'use strict';

window.addEventListener('load', () => {
  document.querySelectorAll('.dc-archive .pagination ul li').forEach((month) => {
    // Get href and look if exists in document
    const link = month.querySelector('a');
    if (link) {
      const href = link.getAttribute('href');
      if (!document.querySelector(href)) {
        const text = link.textContent;
        // Remove link
        month.removeChild(link);
        // Add pure text
        month.textContent = text;
      }
    }
  });
});
