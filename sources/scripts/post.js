'use strict';

window.addEventListener('load', () => {
  let interval;
  const swap = () => {
    const p_remember = document.querySelector('p.remember');
    if (p_remember) {
      const p_site = document.querySelector('p.site-field');
      if (p_site) {
        p_site.insertAdjacentElement('afterend', p_remember);
        clearInterval(interval);
      }
    }
  };
  interval = setInterval(swap, 100);
});
