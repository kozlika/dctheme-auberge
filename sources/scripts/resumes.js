'use strict';

/* sg-comments.js
    Affiche/Masque tous les éléments portant la class="sg-comment"

    Le bouton déclencheur doit être posé dans la source :

    <button id="sg-toggle-comments" class="btn sg-toggle-comments is-closed" arial-expanded="false">commentaires</button>

    (le texte afficher/masquer est introduit via css)
*/
var Scampi = Scampi || {};

Scampi.uComments = function() {

  const toggle = document.getElementById('sg-toggle-comments');
  const comments = document.querySelectorAll('.sg-comment');

  if (!toggle) {
    return;
  }

  function toggleVisibility() {
    // Toggle each comment
    comments.forEach(comment => {
      comment.classList.toggle('is-visible');
    });

    // Toggle main button
    toggle.classList.toggle('is-closed');
    const isExpanded = toggle.getAttribute('aria-expanded') === 'true';
    toggle.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
  }

  toggle.addEventListener('click', toggleVisibility);
};

window.addEventListener('load', () => {
  Scampi.uComments();
});
