# Thème pour le jeu de l'Auberge des Blogueurs

Ce thème est développé pour l'outil de blog [Dotclear](https://dotclear.org). Il servira pour la partie pubique du jeu littéraire [Auberge des Blogueurs](https://auberge.des-blogueurs.org).

Licence : droits réservés pendant la durée du jeu (jusqu'à octobre 2020). Il sera dispo ensuite avec une licence libre.

## Utiliser/Tester ce thème

Quelques éléments sont indispensables pour pouvoir reproduire le fonctionnement de l'auberge sur un blog de test.

### Plugins à installer

* auberge
* contactMe 
* a11yConfig (décocher la case Insertion automatique dans admin/plugin.php?p=a11yConfig)
* commentsWikibar

### Auteurs 

* avoir plusieurs auteurs
* le champ "pseudo" sera celui du personnage -> Il doit être absolument renseigné
* leur attribuer une "chambre" dans leur page d'admin (liste des utilisateurs du blog -> auteur). 
    - 0 ou vide : La Direction
    - 1-999 : les clients
    - 1000 > + : les membres du personnel

### Catégories

* créer une catégorie "Notes de la Direction" (bien respecter la casse)

C'est la seule catégorie qui sera prise en compte à l'affichage.

### Billets

* avoir plusieurs billets par jour sur quelques dates
* varier les auteurs (tous profils)
* affecter quelques billets à la catégorie Note de la Direction (dans le jeu pas plus d'un par jour mais ça n'a pas d'importance pour la mécanique)

## Contribuer au développement

### Prérequis

- [Node.js](https://nodejs.org/)
- gulp 4.xx installé en global

### Initialisation

* Cloner ce dépôt ou le télécharger.
* Lancer la commande `npm install`

### Utilisation

Builder les css et les js : `gulp`

### Tâches Gulp disponibles

* makeCSS : compilation, concaténation et minification des css
* makeJS : concaténation et minification des scripts javascripts
* watchMake : surveillance des sources et exécution des tâches
* gulp : les trois ci-dessus
