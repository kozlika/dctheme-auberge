v1.8
================
- ne pas restituer un bloc d'excerpt si celui-ci est vide 
- réduire l'interlignage pour le nom d'auteur quand celui-ci s'étale sur deux lignes dans les "cartes"
- implémentation des pages pour les contraintes littéraires

v1.7
================
- affichage de l'heure de publication (home, billet seul)
- prise en compte des 'aside' en début de billet pour exclusion de l'amorce

v1.6
================
- décoration des <hr>
- décoration des notes de bas de page

v1.5
================
- ajout de résumés dans les archives (chapo récupéré)
- mise en place du tri par auteur
- modifications subséquentes du footer

v1.4
================
- ajout d'un script permettant la préouverture d'un bloc si une ancre est ajoutée à l'URL.

v1.3.2
================
- ajout de styles pour les citations inline/block
- amélioration des abbr

v1.3 
================
- scroll plus doux
- ajout d'une couleur à la palette pour le staff_7

v1.2
================
- le texte des boutons (dans la FAQ) doit pouvoir être sélectionné
- accessibilité : meilleurs contrastes sur les bordures de champ et le fond des liens d'accès rapide du bac à sable
- accessibilité : meilleure visibilité de la prise du focus
- accessibilité : améliorations du formulaire de contact 

v1.1
================
- correction affichage responsive sur les FAQ
- modif de style pour les pages statiques et modif du footer
- affichage du nb de résultats par page pour la recherche
- correction scroll horizontal

v1.0
================
Prêts pour le grand jour :)
- ajout d'une FAQ


v0.10
================
- meilleure conformité des roles dans le html
- correction bug images max-width:100%
- favicons personnalisables via BlogShortname
- modif image bannière bac à sable (merci Franck Paul)
- fix typo unité em/rem
- remplacement des favicons pour les mêmes mais à fond transparent


v0.9.1
================
- ordre ascendant pour les archives, c'est plus logique
- l'image du header varie entre auberge/bac à sable (nécessite la v0.8 mini du plugin auberge)
- suppression de la balise section.post-feedback vide sur les pages
- améliorations du responsive sur les très petits écrans (inférieurs à 360px)


v0.8.1
================
- typo sur les ancres en bas des archives


v0.8
================
- ajout de la css print


v0.7
================
Prise en compte du plugin a11yconfig (templates et style) : positionnement du bouton en fin de page.

TEMPLATES 
----------------
- ajout des urls dans les liens du footer
- ajout d'un lien d'accès rapide (le premier au clavier au chargement de page) vers la page Accessibilité.

STYLES 
----------------
- amélioration des styles de la commentsWikibar
- correction du textarea qui n'avait pas de bordure quand commentsWikibar était désactivé


v0.6
================

TEMPLATES 
----------------
- pas de commentaires pour la catégorie Notes de la direction
- ajout de la trad "one reaction" dans main.po
- petites modifs dans le footer (wip puisque toutes les pages ne sont pas encore crées)

STYLES
---------------- 
- présentation des notes de la direction façon "post-it" sur home et recherche 
- un peu plus de place pour le nom de perso dans les archives
- ajout de styles pour les "pages"
- styles pour la 404
- correction pour les petits contenus de billets affichés sur un fond différent (notes direction)


v0.5
================

TEMPLATES 
----------------
- le lien d'accès rapide "Aller au menu" va vers le bloc Accueil/Archive
- le lien d'accès rapide "Aller à la recherche" donne le focus dans le champ (sauf pour Firefox, ça ne fonctionne pas)
- le role search est positionné sur le groupe d'inputs au lieu de la boîte enveloppante
- Ajout de l'attribut `role="1"` (balise `{{tpl:AuthorRoom}}` dans `_entry_short.html` et `_entry-content.html`) pour afficher le role du staffeur à la place du numéro de chambre (nécessite la version 0.7+ du plugin auberge, sinon l'attribut sera ignoré)

STYLES 
----------------
- ajustement de la palette des couleurs
- correction du bug qui faisait dépasser le filet de la boîte de prévisu du commentaire 
- meilleure répartition des espaces entre les colonnes du footer
- petits ajustements divers et variés probablement invisibles pour d'autres que moi :)


v0.4
================

TEMPLATES 
----------------
- entête archives: les mois n'ont un lien que s'il comporte des textes publiés
- ContactMe : majuscule au texte du bouton

STYLES 
----------------
- la transparence s'applique désormais toujours sur l'image de header, sauf en home-first
- changement de méthode pour les titres entre filets pour que le fond du titre reste transparent (common.scss)
- petite déco sur les champs et les boutons (common.scss)
- nouveaux styles pour les titres de page (hors billet) pour ne pas confondre avec les "chapter-separator" (common.scss)
- on s'étale un peu plus en largeur dans les archives (archive.scss)
- meilleur calage du bouton de validation prévisu commentaire (feedback.scss)
- regroupement dans layout.scss des règles de positionnement et de fonds des grands blocs
- complément de styles pour l’insertion d’image via <figure>


v0.3
================

TEMPLATES
----------------
- déplacement et reformulation de la checkbox remember me (formulaire de commentaires) ; elle se situe désormais sous les champs d'identification

TRAD 
----------------
- rétablissement de la traduction des boutons Prévisualier/Envoyer, qui avait disparu pour de mystérieuses raisons

STYLES 
----------------
- correction du lien d'accès rapide au menu
- reprise du layout pour une gestion plus simple (définition de la hauteur du header plutôt que réglages par margin du wrapper)
- les archives ne sont plus jamais sur 3 lignes, quelle que soit la résolution
- la nav billet précédent/suivant n'est plus jamais sur deux lignes 
- 1px au lieu de 2 sur la bordure du commentifme
- pas de border-radius sur le côté gauche du bouton de recherche
- un peu de fun sur la prévisualisation d'un commentaire

Environnement de développemement
--------------------------------
Remplacement de uglify par terser pour la compilation/concaténation des scripts



v0.2 - 15/5/202O
================
- correction des bugs remontés sur le pad
