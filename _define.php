<?php
/**
 * @brief Auberge des blogueurs, a theme for https://auberge.des-blogueurs.org
 *
 * @package Dotclear
 * @subpackage Themes
 *
 * @copyright Kozlika
 */

if (!defined('DC_RC_PATH')) {return;}

$this->registerModule(
    "Auberge des blogueurs",       // Name
    "Thème pour l’Auberge des blogueurs",         // Description
    "Dotclear Team",               // Author
    '1.9',                         // Version
    [                              // Properties
        'type'   => 'theme',
        'tplset' => 'dotty'
    ]
);
